import os
import sys
import json
import logging
from bson.json_util import ObjectId
from datetime import datetime
import pymongo
from fastapi import FastAPI, Header, HTTPException, Request
from fastapi.responses import HTMLResponse
import datetime

MONGODB_HOSTNAME = os.getenv("MONGODB_HOSTNAME", "mongodb")
MONGODB_PORT = os.getenv("MONGODB_PORT", "27017")
MONGO_DATABASE = os.getenv("MONGO_DATABASE", "revoappdb")
MONGO_USERNAME = os.getenv("MONGO_USERNAME")
MONGO_PASSWORD = os.getenv("MONGO_PASSWORD")

uri = "mongodb://{}:{}/{}".format(MONGODB_HOSTNAME, MONGODB_PORT, MONGO_DATABASE)
client = pymongo.MongoClient(
    uri,
    username=MONGO_USERNAME,
    password=MONGO_PASSWORD,
    authMechanism='SCRAM-SHA-256'
)
revoappdb = client.revoappdb
collection_revoapp = revoappdb.revoapp

app = FastAPI()

# readiness / liveness check filter out
class EndpointFilter(logging.Filter):
    def filter(self, record: logging.LogRecord) -> bool:
        return record.getMessage().find("/healthz") == -1
# Filter out /healthz
logging.getLogger("uvicorn.access").addFilter(EndpointFilter())

@app.get('/healthz')
def health():
    return ''

@app.get("/")
def read_root():
    return {"info": "revoapp service"}

@app.get("/echo")
def read_root():
    date_now = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')

    return {"echo": date_now}

@app.get("/helloall")
def get_people():
    try:
        resp = collection_revoapp.find()
    except Exception as e:
        return {"resp": 500}

    people = []
    for man in resp:
        man['message'] = bday_msg(man['_id'], man['dateOfBirth'])
        people.append(man)

    response = people

    return response

@app.get("/hello/{username}")
def get_man(username: str):
    key = {'_id' : username}
    try:
        resp = collection_revoapp.find_one(key, {})
    except Exception as e:
        print("Exception: %s" % e)
        return {"resp": 500}

    if resp:
        resp['message'] = bday_msg(resp['_id'], resp['dateOfBirth'])
        return resp
    else:
        return {"resp": "no man found"}

@app.put("/hello/{username}")
def put_man(username, data: dict):
    key = {'_id' : username}
    data = {"$set": {"dateOfBirth": data['dateOfBirth']} }
    try:
        resp = collection_revoapp.update_one(
           key, data, upsert=True
        )
    except Exception as e:
        print("Exception: %s" % e)
        return {"resp": 500}

    query = {"_id": username}
    response = collection_revoapp.find_one(query)

    return response

def bday_msg(username, dateOfBirth):
    now = datetime.datetime.now()
    today = datetime.date.today()
    bday_object = datetime.datetime.strptime(dateOfBirth, '%Y-%m-%d')

    delta1 = datetime.datetime(now.year, bday_object.month, bday_object.day)
    delta2 = datetime.datetime(now.year+1, bday_object.month, bday_object.day)
    days_left = ((delta1 if delta1 > now else delta2) - now).days

    if today.strftime("%d") == bday_object.strftime("%d"):
        msg = "Hello, %s! Happy birthday!" % (username)
    else:
        msg = "Hello, %s! Your birthday is in %s day(s)" % (username, days_left)

    return msg

if __name__ == '__main__':
    main()
