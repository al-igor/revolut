# revapp

I know you requested to put the project to github, but gitlab is easier for me from the perspective of building and hosting docker images which are further used by helm.

### Example of Installation & Testing

Creatings NS, fetching MongoDB chart and installing revapp chart

```
cd revoapp/charts
kubectl create ns revoapp
helm dependency update ./revoapp/ # fetching dependancy chart MongoDB
helm install revoapp -f values-local.yaml ./revoapp -n revoapp
```

Forwarding API port
```
kubectl port-forward deploy/revoapp -n revoapp 8080:80
```

Testing everything is up and running
```
$ curl -s http://localhost:8080/echo | jq .
{
  "echo": "2022-11-27 00:18:19"
}
```


### Example of Using

```
curl -s -d '{ "dateOfBirth": "1988-11-27" }'  -H "Content-Type: application/json" -X PUT http://localhost:8080/hello/igor20 | jq .
```

```
curl -s -d '{ "dateOfBirth": "1988-11-26" }'  -H "Content-Type: application/json" -X PUT http://localhost:8080/hello/igor10 | jq .
```

```
$ curl -s http://localhost:8080/hello/igor10 | jq .
{
  "_id": "igor10",
  "dateOfBirth": "1988-11-26",
  "message": "Hello, igor10! Your birthday is in 363 day(s)"
}
```

```
$ curl -s http://localhost:8080/hello/igor20 | jq .
{
  "_id": "igor20",
  "dateOfBirth": "1988-11-27",
  "message": "Hello, igor20! Happy birthday!"
}
```

```
$ curl -s http://localhost:8080/helloall | jq .
[
  {
    "_id": "igor10",
    "dateOfBirth": "1988-11-26",
    "message": "Hello, igor10! Your birthday is in 363 day(s)"
  },
  {
    "_id": "igor20",
    "dateOfBirth": "1988-11-27",
    "message": "Hello, igor20! Happy birthday!"
  }
]
```

### CI/CD
Im using podman for images build [pipeline](https://gitlab.com/al-igor/revolut/-/blob/main/.gitlab-ci.yml)

### Notes

Sorry, i didnt have time to create tests

### Questions?

email: iskd2600\<at\>gmail.com

-- igor
